;; -*- mode: emacs-lisp -*-
;; This file is loaded by Spacemacs at startup.
;; It must be stored in your home directory.

(defun dotspacemacs/layers ()
  "Configuration Layers declaration.
You should not put any user code in this function besides modifying the variable
values."
  (setq-default
    ;; Base distribution to use. This is a layer contained in the directory
    ;; `+distribution'. For now available distributions are `spacemacs-base'
    ;; or `spacemacs'. (default 'spacemacs)
    dotspacemacs-distribution 'spacemacs
    ;; List of additional paths where to look for configuration layers.
    ;; Paths must have a trailing slash (i.e. `~/.mycontribs/')
    dotspacemacs-configuration-layer-path '("~/spacemacs-layers/")
    ;; List of configuration layers to load. If it is the symbol `all' instead
    ;; of a list then all discovered layers will be installed.
    dotspacemacs-configuration-layers
    '(
       ;; ----------------------------------------------------------------
       ;; Example of useful layers you may want to use right away.
       ;; Uncomment some layer names and press <SPC f e R> (Vim style) or
       ;; <M-m f e R> (Emacs style) to install them.
       ;; ----------------------------------------------------------------
       auto-completion
       better-defaults
       emacs-lisp
       git
       markdown
       org
       (shell :variables
         shell-default-height 30
         shell-default-position 'bottom
         ;; shell-enable-smart-eshell t
         shell-default-shell 'eshell)
       spell-checking
       syntax-checking
       version-control
       ruby
       ruby-on-rails
       osx
       github
       themes-megapack
       html
       javascript
       yaml
       dash
       ranger
       vinegar
       colors
       seeing-is-believing
       gtags
       erc
       quickrun
       php
       ibuffer
       emoji
       fasd
       pandoc
       clojure
       react

       ;; custom layers
       browserify
       clone-github-repo
       check-acronym)

    ;; List of additional packages that will be installed without being
    ;; wrapped in a layer. If you need some configuration for these
    ;; packages, then consider creating a layer. You can also put the
    ;; configuration in `dotspacemacs/user-config'.
    dotspacemacs-additional-packages
    '(editorconfig
       rspec-mode
       minitest
       quickrun
       multiple-cursors
       mc-extras
       narrow-indirect
       dumb-jump
       eshell-autojump
       erc-gitter
       org-alert
       (gh :location (recipe :fetcher github
                       :repo "sigma/gh.el"
                       :commit "248ac04ac1ab0458453f4af52672768fcf8670ec"))
       (pry :location (recipe :fetcher github
                              :repo "jacott/emacs-pry"
                              :branch "master")))
    ;; A list of packages and/or extensions that will not be install and loaded.
    dotspacemacs-excluded-packages '()
    ;; If non-nil spacemacs will delete any orphan packages, i.e. packages that
    ;; are declared in a layer which is not a member of
    ;; the list `dotspacemacs-configuration-layers'. (default t)
    dotspacemacs-delete-orphan-packages t))

(defun dotspacemacs/init ()
  "Initialization function.
This function is called at the very startup of Spacemacs initialization
before layers configuration.
You should not put any user code in there besides modifying the variable
values."
  ;; This setq-default sexp is an exhaustive list of all the supported
  ;; spacemacs settings.
  (setq-default
    ;; If non nil ELPA repositories are contacted via HTTPS whenever it's
    ;; possible. Set it to nil if you have no way to use HTTPS in your
    ;; environment, otherwise it is strongly recommended to let it set to t.
    ;; This variable has no effect if Emacs is launched with the parameter
    ;; `--insecure' which forces the value of this variable to nil.
    ;; (default t)
    dotspacemacs-elpa-https t
    ;; Maximum allowed time in seconds to contact an ELPA repository.
    dotspacemacs-elpa-timeout 5
    ;; If non nil then spacemacs will check for updates at startup
    ;; when the current branch is not `develop'. (default t)
    dotspacemacs-check-for-update t
    ;; One of `vim', `emacs' or `hybrid'. Evil is always enabled but if the
    ;; variable is `emacs' then the `holy-mode' is enabled at startup. `hybrid'
    ;; uses emacs key bindings for vim's insert mode, but otherwise leaves evil
    ;; unchanged. (default 'vim)
    dotspacemacs-editing-style 'vim
    ;; If non nil output loading progress in `*Messages*' buffer. (default nil)
    dotspacemacs-verbose-loading nil
    ;; Specify the startup banner. Default value is `official', it displays
    ;; the official spacemacs logo. An integer value is the index of text
    ;; banner, `random' chooses a random text banner in `core/banners'
    ;; directory. A string value must be a path to an image format supported
    ;; by your Emacs build.
    ;; If the value is nil then no banner is displayed. (default 'official)
    dotspacemacs-startup-banner 'official
    ;; List of items to show in the startup buffer. If nil it is disabled.
    ;; Possible values are: `recents' `bookmarks' `projects'.
    ;; (default '(recents projects))
    dotspacemacs-startup-lists '(recents projects bookmarks)
    ;; Number of recent files to show in the startup buffer. Ignored if
    ;; `dotspacemacs-startup-lists' doesn't include `recents'. (default 5)
    dotspacemacs-startup-recent-list-size 5
    ;; Default major mode of the scratch buffer (default `text-mode')
    dotspacemacs-scratch-mode 'text-mode
    ;; List of themes, the first of the list is loaded when spacemacs starts.
    ;; Press <SPC> T n to cycle to the next theme in the list (works great
    ;; with 2 themes variants, one dark and one light)
    dotspacemacs-themes '(spacemacs-dark
                           darkokai
                           spacemacs-light
                           solarized-light
                           solarized-dark
                           leuven
                           monokai
                           zenburn
                           molokai
                           flatland
                           junio
                           spolsky
                           clues
                           lush
                           noctilux
                           sanityinc-tomorrow-night
                           smyx
                           wombat
                           seti)
    ;; If non nil the cursor color matches the state color in GUI Emacs.
    dotspacemacs-colorize-cursor-according-to-state t
    ;; Default font. `powerline-scale' allows to quickly tweak the mode-line
    ;; size to make separators look not too crappy.
    dotspacemacs-default-font '("Input Mono"
                                 :size 11
                                 :weight normal
                                 :width normal
                                 :powerline-scale 1.1)
    ;; The leader key
    dotspacemacs-leader-key "SPC"
    ;; The leader key accessible in `emacs state' and `insert state'
    ;; (default "M-m")
    dotspacemacs-emacs-leader-key "M-m"
    ;; Major mode leader key is a shortcut key which is the equivalent of
    ;; pressing `<leader> m`. Set it to `nil` to disable it. (default ",")
    dotspacemacs-major-mode-leader-key ","
    ;; Major mode leader key accessible in `emacs state' and `insert state'.
    ;; (default "C-M-m)
    dotspacemacs-major-mode-emacs-leader-key "C-M-m"
    ;; These variables control whether separate commands are bound in the GUI to
    ;; the key pairs C-i, TAB and C-m, RET.
    ;; Setting it to a non-nil value, allows for separate commands under <C-i>
    ;; and TAB or <C-m> and RET.
    ;; In the terminal, these pairs are generally indistinguishable, so this only
    ;; works in the GUI. (default nil)
    dotspacemacs-distinguish-gui-tab t
    ;; (Not implemented) dotspacemacs-distinguish-gui-ret nil
    ;; The command key used for Evil commands (ex-commands) and
    ;; Emacs commands (M-x).
    ;; By default the command key is `:' so ex-commands are executed like in Vim
    ;; with `:' and Emacs commands are executed with `<leader> :'.
    dotspacemacs-command-key ":"
    ;; If non nil `Y' is remapped to `y$'. (default t)
    dotspacemacs-remap-Y-to-y$ t
    ;; Name of the default layout (default "Default")
    dotspacemacs-default-layout-name "Default"
    ;; If non nil the default layout name is displayed in the mode-line.
    ;; (default nil)
    dotspacemacs-display-default-layout nil
    ;; If non nil then the last auto saved layouts are resume automatically upon
    ;; start. (default nil)
    dotspacemacs-auto-resume-layouts nil
    ;; Location where to auto-save files. Possible values are `original' to
    ;; auto-save the file in-place, `cache' to auto-save the file to another
    ;; file stored in the cache directory and `nil' to disable auto-saving.
    ;; (default 'cache)
    dotspacemacs-auto-save-file-location 'cache
    ;; Maximum number of rollback slots to keep in the cache. (default 5)
    dotspacemacs-max-rollback-slots 5
    ;; If non nil then `ido' replaces `helm' for some commands. For now only
    ;; `find-files' (SPC f f), `find-spacemacs-file' (SPC f e s), and
    ;; `find-contrib-file' (SPC f e c) are replaced. (default nil)
    dotspacemacs-use-ido nil
    ;; If non nil, `helm' will try to minimize the space it uses. (default nil)
    dotspacemacs-helm-resize t
    ;; if non nil, the helm header is hidden when there is only one source.
    ;; (default nil)
    dotspacemacs-helm-no-header nil
    ;; define the position to display `helm', options are `bottom', `top',
    ;; `left', or `right'. (default 'bottom)
    dotspacemacs-helm-position 'bottom
    ;; If non nil the paste micro-state is enabled. When enabled pressing `p`
    ;; several times cycle between the kill ring content. (default nil)
    dotspacemacs-enable-paste-micro-state nil
    ;; Which-key delay in seconds. The which-key buffer is the popup listing
    ;; the commands bound to the current keystroke sequence. (default 0.4)
    dotspacemacs-which-key-delay 0.4
    ;; Which-key frame position. Possible values are `right', `bottom' and
    ;; `right-then-bottom'. right-then-bottom tries to display the frame to the
    ;; right; if there is insufficient space it displays it at the bottom.
    ;; (default 'bottom)
    dotspacemacs-which-key-position 'bottom
    ;; If non nil a progress bar is displayed when spacemacs is loading. This
    ;; may increase the boot time on some systems and emacs builds, set it to
    ;; nil to boost the loading time. (default t)
    dotspacemacs-loading-progress-bar t
    ;; If non nil the frame is fullscreen when Emacs starts up. (default nil)
    ;; (Emacs 24.4+ only)
    dotspacemacs-fullscreen-at-startup nil
    ;; If non nil `spacemacs/toggle-fullscreen' will not use native fullscreen.
    ;; Use to disable fullscreen animations in OSX. (default nil)
    dotspacemacs-fullscreen-use-non-native nil
    ;; If non nil the frame is maximized when Emacs starts up.
    ;; Takes effect only if `dotspacemacs-fullscreen-at-startup' is nil.
    ;; (default nil) (Emacs 24.4+ only)
    dotspacemacs-maximized-at-startup t
    ;; A value from the range (0..100), in increasing opacity, which describes
    ;; the transparency level of a frame when it's active or selected.
    ;; Transparency can be toggled through `toggle-transparency'. (default 90)
    dotspacemacs-active-transparency 90
    ;; A value from the range (0..100), in increasing opacity, which describes
    ;; the transparency level of a frame when it's inactive or deselected.
    ;; Transparency can be toggled through `toggle-transparency'. (default 90)
    dotspacemacs-inactive-transparency 90
    ;; If non nil unicode symbols are displayed in the mode line. (default t)
    dotspacemacs-mode-line-unicode-symbols t
    ;; If non nil smooth scrolling (native-scrolling) is enabled. Smooth
    ;; scrolling overrides the default behavior of Emacs which recenters the
    ;; point when it reaches the top or bottom of the screen. (default t)
    dotspacemacs-smooth-scrolling t
    ;; If non nil line numbers are turned on in all `prog-mode' and `text-mode'
    ;; derivatives. If set to `relative', also turns on relative line numbers.
    ;; (default nil)
    dotspacemacs-line-numbers t
    ;; If non-nil smartparens-strict-mode will be enabled in programming modes.
    ;; (default nil)
    dotspacemacs-smartparens-strict-mode nil
    ;; Select a scope to highlight delimiters. Possible values are `any',
    ;; `current', `all' or `nil'. Default is `all' (highlight any scope and
    ;; emphasis the current one). (default 'all)
    dotspacemacs-highlight-delimiters 'all
    ;; If non nil advises quit functions to keep server open when quitting.
    ;; (default nil)
    dotspacemacs-persistent-server nil
    ;; List of search tool executable names. Spacemacs uses the first installed
    ;; tool of the list. Supported tools are `ag', `pt', `ack' and `grep'.
    ;; (default '("ag" "pt" "ack" "grep"))
    dotspacemacs-search-tools '("ag" "pt" "ack" "grep")
    ;; The default package repository used if no explicit repository has been
    ;; specified with an installed package.
    ;; Not used for now. (default nil)
    dotspacemacs-default-package-repository nil
    ;; Delete whitespace while saving buffer. Possible values are `all'
    ;; to aggressively delete empty line and long sequences of whitespace,
    ;; `trailing' to delete only the whitespace at end of lines, `changed'to
    ;; delete only whitespace for changed lines or `nil' to disable cleanup.
    ;; (default nil)
    dotspacemacs-whitespace-cleanup nil
    ))

(defun dotspacemacs/user-init ()
  "Initialization function for user code.
It is called immediately after `dotspacemacs/init', before layer configuration
executes.
 This function is mostly useful for variables that need to be set
before packages are loaded. If you are unsure, you should try in setting them in
`dotspacemacs/user-config' first."
  (setq-default ruby-version-manager 'rbenv))

(defun dotspacemacs/user-config ()
  "Configuration function for user code.
This function is called at the very end of Spacemacs initialization after
layers configuration.
This is the place where most of your configurations should be done. Unless it is
explicitly specified that a variable should be set before a package is loaded,
you should place you code here."

  (defadvice multi-term (after advise-multi-term-coding-system)
    (set-buffer-process-coding-system 'utf-8-unix 'utf-8-unix))
  (ad-activate 'multi-term)
  (prefer-coding-system 'utf-8)
  (setq system-uses-terminfo nil)

  (require 'quickrun)

  (require 'rspec-mode)
  (defadvice rspec-compile (around rspec-compile-around)
    "Use BASH shell for running the specs because of ZSH issues."
    (let ((shell-file-name "/bin/bash"))
      ad-do-it))

  (ad-activate 'rspec-compile)

  (eval-after-load 'rspec-mode
    '(rspec-install-snippets))

  (add-hook 'after-init-hook 'inf-ruby-switch-setup)

  (require 'minitest)

  (setq-default evil-shift-width 2)
  (setq js-indent-level 2)

  (eval-after-load 'ruby-mode
    (progn
      ;; (evil-leader/set-key-for-mode 'ruby-mode "mtra" 'rspec-verify-all)
      ;; (evil-leader/set-key-for-mode 'ruby-mode "mtrs" 'rspec-verify-single)
      (evil-leader/set-key-for-mode 'ruby-mode "mtma" 'minitest-verify-all)
      (evil-leader/set-key-for-mode 'ruby-mode "mtms" 'minitest-verify-single)
      (evil-leader/set-key-for-mode 'ruby-mode "mrq"  'quickrun-shell)))

  (setq compilation-scroll-output 'first-error)

  (require 'multiple-cursors)
  (global-set-key (kbd "C->") 'mc/mark-next-like-this)
  (global-set-key (kbd "C-<") 'mc/mark-previous-like-this)
  (global-set-key (kbd "C-c C-<") 'mc/mark-all-like-this)
  ;; Lisp specific defuns

  (defun eval-and-replace ()
    "Replace the preceding sexp with its value."
    (interactive)
    (backward-kill-sexp)
    (condition-case nil
      (prin1 (eval (read (current-kill 0)))
        (current-buffer))
      (error (message "Invalid expression")
        (insert (current-kill 0)))))

  (defun bc/toggle-implementation-and-test-in-window ()
    (interactive)
    (if (or
          (file-exists-p (concat (projectile-project-root) "spec/spec_helper.rb"))
          (file-exists-p (concat (projectile-project-root) "spec/rails_helper.rb")))
      (projectile-rails-find-current-spec)
      (projectile-toggle-between-implementation-and-test)))

  (defun bc/toggle-implementation-and-test-other-window ()
    (interactive)
    (if (or
          (file-exists-p (concat (projectile-project-root) "spec/spec_helper.rb"))
          (file-exists-p (concat (projectile-project-root) "spec/rails_helper.rb")))
      (rspec-find-spec-or-target-other-window)
      (projectile-find-implementation-or-test-other-window)))

  (spacemacs/set-leader-keys "pa" 'bc/toggle-implementation-and-test-in-window)
  (spacemacs/set-leader-keys "pA" 'bc/toggle-implementation-and-test-other-window)

  (setq delete-by-moving-to-trash t)

  ;; (add-hook 'after-change-major-mode-hook 'editorconfig-apply 'append)

  (require 'editorconfig)
  (editorconfig-mode 1)

  ;; (spacemacs/helm-gtags-define-keys-for-mode 'ruby-mode)
  ;; (spacemacs/helm-gtags-define-keys-for-mode 'rspec-mode)
  ;; (with-eval-after-load 'robe
  ;;   (spacemacs/set-leader-keys-for-major-mode 'ruby-mode "gg" 'helm-gtags-dwim)
  ;;   (spacemacs/set-leader-keys-for-major-mode 'rspec-mode "gg" 'helm-gtags-dwim))

  (setq mc/list-file "~/.mc-lists.el")
  (setq yas-snippet-dirs (append yas-snippet-dirs
                           '("~/spacemacs-snippets")))
  (yas-reload-all)
  (setq create-lockfiles nil)

  ;; Fix the jump when cursor is at a line between 0 and 5 and you scroll
  (setq scroll-margin 5)

  ;; Start with 3 windows
  ;; (split-window-right-and-focus)
  ;; (spacemacs/default-pop-shell)
  ;; (select-window-1)
  ;; (jjjl-window-move-far-left)

  (spacemacs/set-leader-keys "g2c" 'bc/clone-github-repo)
  (spacemacs/set-leader-keys "g2C" (lambda () (interactive) (bc/clone-github-repo 't)))

  (add-hook 'before-save-hook 'whitespace-cleanup)
  ;; (add-hook 'before-save-hook
  ;;   (lambda ()
  ;;     (when
  ;;       (not
  ;;         (derived-mode-p 'markdown-mode 'yaml-mode))
  ;;       (spacemacs/indent-region-or-buffer))))

  (setq projectile-require-project-root nil)

  ;; narrow-indirect bindings
  (define-key ctl-x-4-map "nd" 'ni-narrow-to-defun-other-window)
  (define-key ctl-x-4-map "nn" 'ni-narrow-to-region-other-window)
  (define-key ctl-x-4-map "np" 'ni-narrow-to-page-other-window)

  (spacemacs|define-custom-layout "Launch School"
    :binding "l"
    :body
    (find-file "~/dev/railstutors")
    (projectile-rails-server)
    (projectile-rails-console nil)
    (run-at-time "2 sec" nil '(lambda ()
                                (gusp/browserify "3000")
                                (switch-to-buffer "railstutors")
                                (spacemacs/toggle-maximize-buffer)
                                (helm-projectile-find-file))))
  (setq ranger-override-dired t)
  (add-hook 'after-init-hook 'global-company-mode)
  (dumb-jump-mode)

  (eval-after-load 'eshell
    '(require 'eshell-autojump nil t))

  (setq eshell-last-dir-ring-size 500)

  ;; (when (not (getenv "EMACS_NO_APPLE_KEYBOARD"))
  ;;   ;; Set keys for Apple keyboard, for emacs in OS X
  ;;   (setq mac-command-modifier 'meta)
  ;;   (setq mac-option-modifier  'super)
  ;;   (setq mac-control-modifier 'control)
  ;;   (setq ns-function-modifier 'hyper))

  (spacemacs/set-leader-keys "oc" 'seeing-is-believing-mark-current-line-for-xmpfilter)

  ;; Quick character jump
  (spacemacs/set-leader-keys "SPC" #'avy-goto-char-timer)

  (setq org-capture-templates
    '(("t" "Todo" entry (file+headline "~/org/gtd.org" "Tasks")
        "* TODO %?\n  %i\n  %U\n  %a")
       ("n" "Note" entry (file+datetree "~/org/notes.org")
         "* %?\n  %U\n  %i\n  %a")
       ("l" "Launch School Note" entry (file+headline "~/org/launch_school.org" "Notes")
         "* %?\n  %U\n  %i\n  %a")
       ("L" "Launch School Todo" entry (file+headline "~/org/launch_school.org" "Tasks")
         "* TODO %?\n  %U\n  %i\n  %a")
       ("d" "Dave Note" entry (file+headline "~/org/dave.org" "Notes")
         "* %?\n  %U\n  %i\n  %a")
       ("D" "Dave Todo" entry (file+headline "~/org/dave.org" "Tasks")
         "* TODO %?\n  %U\n  %i\n  %a")
       ("e" "Eric Note" entry (file+headline "~/org/eric.org" "Notes")
         "* %?\n  %U\n  %i\n  %a")
       ("E" "Eric Todo" entry (file+headline "~/org/eric.org" "Tasks")
         "* TODO %?\n  %U\n  %i\n  %a")
       ("k" "Kim Note" entry (file+headline "~/org/kim.org" "Notes")
         "* %?\n  %U\n  %i\n  %a")
       ("K" "Kim Todo" entry (file+headline "~/org/kim.org" "Tasks")
         "* TODO %?\n  %U\n  %i\n  %a")
       ("c" "Coin Shop Note" entry (file+headline "~/org/coin_shop.org" "Notes")
         "* %?\n  %U\n  %i\n  %a")
       ("C" "Coin Shop Todo" entry (file+headline "~/org/coin_shop.org" "Tasks")
         "* TODO %?\n  %U\n  %i\n  %a")
       ("m" "Movie to Watch" entry (file+headline "~/org/movies.org" "Movies")
         "* %?\n  %U\n  %i ")))


  (defun bc/enable-org-alert ()
    (require 'org-alert)
    (setq alert-default-style 'notifier)
    (org-agenda-list)
    (quit-window)
    (org-alert-check))

  (bc/enable-org-alert)

  avy-all-windows 'all-frames

  (fset 'evil-visual-update-x-selection 'ignore))

;; Do not write anything past this comment. This is where Emacs will
;; auto-generate custom variable definitions.
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(flycheck-javascript-standard-executable "semistandard")
 '(org-agenda-files (quote ("~/org")))
  '(package-selected-packages
     (quote
       (wgrep smex ivy-hydra flyspell-correct-ivy counsel-projectile counsel-dash counsel swiper ivy uuidgen powerline osx-dictionary org-projectile org org-download alert log4e gntp mwim livid-mode skewer-mode simple-httpd link-hint json-snatcher json-reformat parent-mode request haml-mode gitignore-mode github-search fringe-helper git-gutter+ marshal ht gh logito pcache flyspell-correct-helm flyspell-correct pos-tip flx grizzl evil-visual-mark-mode evil-unimpaired magit-popup evil-ediff anzu evil goto-chg eshell-z php-mode diminish darkokai-theme web-completion-data dash-functional tern company column-enforce-mode color-identifiers-mode clojure-snippets hydra multiple-cursors peg eval-sexp-fu highlight pkg-info epl inf-ruby bind-map bind-key yasnippet packed dash async auto-complete popup package-build web-mode smooth-scrolling ruby-test-mode robe ranger projectile-rails persp-mode paradox organic-green-theme org-plus-contrib omtose-phellack-theme neotree move-text macrostep leuven-theme less-css-mode js2-refactor js2-mode help-fns+ helm-themes helm-projectile helm-gtags helm-flyspell helm-ag gruvbox-theme grandshell-theme git-gutter-fringe ggtags evil-mc eshell-prompt-extras dumb-jump darktooth-theme cyberpunk-theme color-theme-sanityinc-tomorrow clj-refactor cider badwolf-theme auto-yasnippet ample-theme ace-window ace-link ace-jump-helm-line iedit smartparens undo-tree flycheck helm helm-core projectile markdown-mode magit git-commit with-editor f s zonokai-theme zenburn-theme zen-and-art-theme yaml-mode xterm-color ws-butler window-numbering which-key web-beautify volatile-highlights vi-tilde-fringe use-package underwater-theme ujelly-theme twilight-theme twilight-bright-theme twilight-anti-bright-theme tronesque-theme toxi-theme toc-org tao-theme tangotango-theme tango-plus-theme tango-2-theme tagedit sunny-day-theme sublime-themes subatomic256-theme subatomic-theme stekene-theme spinner spacemacs-theme spaceline spacegray-theme soothe-theme soft-stone-theme soft-morning-theme soft-charcoal-theme smyx-theme smeargle slim-mode shell-pop seti-theme seq seeing-is-believing scss-mode sass-mode rvm ruby-tools rubocop rspec-mode reverse-theme reveal-in-osx-finder restart-emacs rbenv rake rainbow-mode rainbow-identifiers rainbow-delimiters railscasts-theme quickrun queue quelpa purple-haze-theme pry professional-theme popwin planet-theme phpunit phpcbf php-extras php-auto-yasnippets phoenix-dark-pink-theme phoenix-dark-mono-theme pcre2el pbcopy pastels-on-dark-theme paredit pandoc-mode page-break-lines ox-pandoc osx-trash orgit org-repo-todo org-present org-pomodoro org-bullets org-alert open-junk-file oldlace-theme occidental-theme obsidian-theme noctilux-theme niflheim-theme narrow-indirect naquadah-theme mustang-theme multi-term monokai-theme monochrome-theme molokai-theme moe-theme mmm-mode minitest minimal-theme mc-extras material-theme markdown-toc majapahit-theme magit-gitflow magit-gh-pulls lush-theme lorem-ipsum linum-relative light-soap-theme let-alist launchctl json-mode js-doc jbeans-theme jazz-theme jade-mode ir-black-theme inkpot-theme info+ inflections indent-guide ido-vertical-mode ibuffer-projectile hungry-delete htmlize hl-todo highlight-parentheses highlight-numbers highlight-indentation heroku-theme hemisu-theme helm-swoop helm-mode-manager helm-make helm-gitignore helm-flx helm-descbinds helm-dash helm-css-scss helm-company helm-c-yasnippet hc-zenburn-theme gruber-darker-theme gotham-theme google-translate golden-ratio gnuplot github-clone github-browse-file gitconfig-mode gitattributes-mode git-timemachine git-messenger git-link git-gutter-fringe+ git-gutter gist gh-md gandalf-theme flycheck-pos-tip flx-ido flatui-theme flatland-theme firebelly-theme fill-column-indicator feature-mode fasd farmhouse-theme fancy-battery eyebrowse expand-region exec-path-from-shell evil-visualstar evil-tutor evil-surround evil-search-highlight-persist evil-numbers evil-nerd-commenter evil-matchit evil-magit evil-lisp-state evil-indent-plus evil-iedit-state evil-exchange evil-escape evil-args evil-anzu espresso-theme eshell-autojump esh-help erc-yt erc-view-log erc-terminal-notifier erc-social-graph erc-image erc-hl-nicks erc-gitter emoji-cheat-sheet-plus emmet-mode elisp-slime-nav edn editorconfig drupal-mode dracula-theme django-theme diff-hl define-word dash-at-point darkmine-theme darkburn-theme dakrone-theme company-web company-tern company-statistics company-quickhelp company-emoji colorsarenice-theme color-theme-sanityinc-solarized coffee-mode clues-theme clojure-mode clean-aindent-mode cider-eval-sexp-fu chruby cherry-blossom-theme busybee-theme bundler buffer-move bubbleberry-theme bracketed-paste birds-of-paradise-plus-theme avy auto-highlight-symbol auto-dictionary auto-compile apropospriate-theme anti-zenburn-theme ample-zen-theme alect-themes aggressive-indent afternoon-theme adaptive-wrap ac-ispell))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
