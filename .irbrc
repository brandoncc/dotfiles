require 'irb/completion'
require 'irb/ext/save-history'

#History configuration
IRB.conf[:SAVE_HISTORY] = 10000
IRB.conf[:HISTORY_FILE] = "#{ENV['HOME']}/.irb-history"

if ENV['RAILS_ENV']
  begin
    require 'hirb'
  rescue LoadError
    # Missing goodies, bummer
  end

  if defined? Hirb
    Hirb.enable
  end
end
