# Path to your oh-my-zsh installation.
export ZSH=$HOME/.oh-my-zsh

# Set name of the theme to load.
# Look in ~/.oh-my-zsh/themes/
# Optionally, if you set this to "random", it'll load a random theme each
# time that oh-my-zsh is loaded.
ZSH_THEME="fino"

# Example aliases
alias subl="/Applications/Sublime\ Text.app/Contents/SharedSupport/bin/subl"
alias zshconfig="vim ~/.zshrc"
alias ohmyzsh="vim ~/.oh-my-zsh"
alias ngrok="~/bin/ngrok http -subdomain=brandoncc"
alias r="rspec --format Fivemat"
alias zshreload="source ~/.zshrc"
alias tmux="TERM=xterm-256color tmux"
alias tml="tmux list-sessions"
alias tma="tmux -2 attach -t $1"
alias tmk="tmux kill-session -t $1"
alias tmcc="tmux -CC attach"
alias tmn="tmux new -s $1"
alias tmr="tmux rename-session -t $1 $2"
alias acronym="ruby ~/dev/scratchbox/find_acronym.rb $1"
alias find_repo="ruby ~/dev/scratchbox/find_git_repo.rb $1 $2"
alias dbyml="wget https://gist.githubusercontent.com/brandoncc/48a869862cdeed5a130b/raw/8491d5cba32f08df8563c155a7b6c42570693986/myflix_db.yml -O config/database.yml"
alias myflix="bi && dbyml && rake db:reset && cp ~/dev/build_robust_and_production_quality_applications/myflix/config/application.yml config/application.yml"
alias pw="ruby ~/dev/password_generator/password_generator.rb"
alias tlbooks="rake books:build && rake chapters:build_all"
alias gpr="git pull --rebase"
alias bs="browser-sync start --proxy \"localhost:3000\" --files \"app/assets/**/*.*\" --files \"app/views/**/*.*\""
alias cgf="TARGET_DIRECTORY=~/students EDITOR=/usr/local/Cellar/macvim/7.4-84/MacVim.app/Contents/MacOS/Vim clone_git_file $1"
alias emacsgui="/usr/local/Cellar/emacs-mac/emacs-24.5-z-mac-5.15/Emacs.app/Contents/MacOS/Emacs"
alias mux="tmuxinator start $1"
# alias open_gf="TARGET_DIRECTORY=~/students EDITOR=/usr/local/Cellar/macvim/7.4-84/MacVim.app/Contents/MacOS/Vim clone_git_file -ts $1"

function ogf () {
  echo "Cloning, your editor will open when clone has completed..."
  source <(TARGET_DIRECTORY=~/students EDITOR=$EDITOR clone_git_file -ts "$1")
}

function vogf () {
  EDITOR=vim ogf "$1"
}

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Update automatically
DISABLE_UPDATE_PROMPT=true

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to disable command auto-correction.
# DISABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
plugins=(git autojump brew bundler gem last-working-dir powify rails vagrant github zsh-syntax-highlighting heroku vim zsh-nvm)

source $ZSH/oh-my-zsh.sh

# User configuration

export PATH="$PATH:/Users/brandoncc/bin:/usr/bin:/bin:/usr/sbin:/sbin:/usr/local/bin"
export PATH="$(brew --prefix qt@5.5)/bin:$PATH"
# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/dsa_id"

# required for cli tools to work with Postgres.app
export PATH=$PATH:/Applications/Postgres.app/Contents/Versions/latest/bin
[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh


# use vim as editor
export EDITOR=code

# use tmuxinator
source ~/bin/tmuxinator.zsh

fpath=(~/.zsh/zsh-completions/src $fpath)
export PATH="$HOME/.rbenv/bin:$PATH"
if which rbenv > /dev/null; then eval "$(rbenv init -)"; fi

export PATH="/Users/brandoncc/.cask/bin:$PATH" # Add cask for emacs
export PATH="$HOME/.composer/vendor/bin:$PATH"
export PATH="/usr/local/heroku/bin:$PATH"

export PATH="$PATH:`yarn global bin`"

export ANDROID_HOME=~/Library/Android/sdk
export PATH=${PATH}:${ANDROID_HOME}/tools
export PATH=${PATH}:${ANDROID_HOME}/platform-tools

###
# percol setup
###
function exists { which $1 &> /dev/null }

if exists percol; then
    function percol_select_history() {
        local tac
        exists gtac && tac="gtac" || { exists tac && tac="tac" || { tac="tail -r" } }
        BUFFER=$(fc -l -n 1 | eval $tac | percol --query "$LBUFFER")
        CURSOR=$#BUFFER         # move cursor
        zle -R -c               # refresh
    }

    zle -N percol_select_history
    bindkey '^R' percol_select_history
fi

# bindkey "\e[H" beginning-of-line
# bindkey "\e[F" end-of-line
# bindkey "\e[1;5D" backward-word
# bindkey "\e[1;5C" forward-word
# bindkey "^[OD" backward-word
# bindkey "^[OC" forward-word

test -e "${HOME}/.iterm2_shell_integration.zsh" && source "${HOME}/.iterm2_shell_integration.zsh"
export PATH="/usr/local/opt/mysql@5.5/bin:$PATH"
export PATH="/usr/local/opt/imagemagick@6/bin:$PATH"
export LDFLAGS="-L/usr/local/opt/imagemagick@6/lib"
export CPPFLAGS="-I/usr/local/opt/imagemagick@6/include"
# export LDFLAGS="-L/usr/local/opt/readline/lib"
# export CPPFLAGS="-I/usr/local/opt/readline/include"
export PKG_CONFIG_PATH="/usr/local/opt/imagemagick@6/lib/pkgconfig"
[[ -s "$HOME/.avn/bin/avn.sh" ]] && source "$HOME/.avn/bin/avn.sh" # load avn
