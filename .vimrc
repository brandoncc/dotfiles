set nocompatible
filetype off

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#rc()

Plugin 'gmarik/Vundle.vim'
Plugin 'tpope/vim-repeat'

Plugin 'tpope/vim-fugitive'
Plugin 'idanarye/vim-merginal'
Plugin 'tpope/vim-rails'
Plugin 'tpope/vim-surround'
Plugin 'tpope/vim-sensible'
Plugin 'tpope/vim-ragtag'
Plugin 'tmhedberg/matchit'
Plugin 'tpope/vim-endwise'
Plugin 'tpope/vim-commentary'
" Plugin 'tpope/vim-markdown'
Plugin 'tpope/vim-haml'
Plugin 'mustache/vim-mustache-handlebars'
Plugin 'vim-ruby/vim-ruby'
Plugin 'jgdavey/vim-blockle'
Plugin 'junegunn/vim-easy-align'
Plugin 'kchmck/vim-coffee-script'
Plugin 'pangloss/vim-javascript'
Plugin 'thoughtbot/vim-rspec'
Plugin 'tpope/vim-rbenv'

Plugin 'mileszs/ack.vim'
Plugin 'ggreer/the_silver_searcher'
Plugin 'scrooloose/nerdtree'
Plugin 'Lokaltog/vim-easymotion'
Plugin 'duff/vim-bufonly'
Plugin 'nathanaelkane/vim-indent-guides'
Plugin 'mattn/webapi-vim'
Plugin 'mattn/gist-vim'
Plugin 'bling/vim-airline'

" vim-textobj-rubyblock depends on vim-textobj-user
Plugin 'kana/vim-textobj-user'
Plugin 'nelstrom/vim-textobj-rubyblock'

Plugin 'scrooloose/syntastic'
Plugin 'rizzatti/dash.vim'
Plugin 'mattn/emmet-vim'
Plugin 'slim-template/vim-slim'
Plugin 'wesQ3/vim-windowswap'
Plugin 'tpope/vim-eunuch'
Plugin 'terryma/vim-multiple-cursors'
Plugin 'editorconfig/editorconfig-vim'
Plugin 'majutsushi/tagbar'
Plugin 'digitaltoad/vim-jade'
Plugin 'Shutnik/jshint2.vim'
Plugin 'trusktr/seti.vim'
Plugin 'ngmy/vim-rubocop'
Plugin 'hwartig/vim-seeing-is-believing'
Plugin 'mxw/vim-jsx'
Plugin 'vim-scripts/loremipsum'
Plugin 'wavded/vim-stylus'
Plugin 'posva/vim-vue'

Plugin 'benmills/vimux'
Plugin 'janko-m/vim-test'
Plugin 'jceb/vim-orgmode'
Plugin 'dhruvasagar/vim-table-mode'

Plugin 'SirVer/ultisnips'
Plugin 'honza/vim-snippets'
Plugin 'Valloric/YouCompleteMe'
Plugin 'szw/vim-tags'

Plugin 'sjbach/lusty'
Plugin 'sjl/gundo.vim'

Plugin 'tpope/vim-obsession'
" Plugin 'jtratner/vim-flavored-markdown'

Plugin 'godlygeek/tabular'
Plugin 'plasticboy/vim-markdown'

Plugin 'junegunn/fzf'
Plugin 'junegunn/fzf.vim'

Plugin 'qpkorr/vim-bufkill'

Plugin 'kana/vim-fakeclip'
Plugin 'ElmCast/elm-vim'

Plugin 'airblade/vim-rooter'

Plugin 'briancollins/vim-jst'

" Start vim-snipmate plugins
" Plugin 'MarcWeber/vim-addon-mw-utils'
" Plugin 'tomtom/tlib_vim'
" Plugin 'garbas/vim-snipmate'
" Plugin 'rcyrus/snipmate-snippets-rubymotion'

" Optional:
" Plugin 'honza/vim-snippets'
" End vim-snipmate plugins

syntax on
filetype plugin indent on

set visualbell
set wildmenu
set wildmode=list:longest,full
set splitright
set splitbelow
set hidden
set guifont=Monaco:h16
set guioptions-=T guioptions-=e guioptions-=L guioptions-=r
set shell=bash
set background=dark
set tabstop=2
set shiftwidth=2
set expandtab
set nofoldenable
set ignorecase smartcase
set autoindent
set backspace=indent,eol,start
set list
set number
let g:gist_post_private=1
set cpoptions+=$
set showmatch
set matchpairs+=<:>
set colorcolumn=80

iabbrev bpry require 'pry'; binding.pry;
iabbrev jq1 //code.jquery.com/jquery-1.11.3.min.js
iabbrev jq2 //code.jquery.com/jquery-2.1.4.min.js

augroup vimrc
  autocmd!
  autocmd GuiEnter * set columns=120 lines=70 number
augroup END

au BufWritePost .vimrc so ~/.vimrc
autocmd QuickFixCmdPost *grep* cwindow

colorscheme seti

" Resize window splits
" TODO Fix mousewheel
nnoremap <Up>    3<C-w>-
nnoremap <Down>  3<C-w>+
nnoremap <Left>  3<C-w><
nnoremap <Right> 3<C-w>>

" Navigate splits easily
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l
map <C-h> <C-w>h

" Create splits easily
nnoremap _ :split<cr>
nnoremap \| :vsplit<cr>

" shift+k -> like shift+j, but no extra space
noremap <S-k> gJ

" Write file when you forget to use sudo
cmap w!! w !sudo tee % >/dev/null

let mapleader = ","
vmap D y'>p
imap <c-l> <space>=><space>
imap <c-c> <esc>
" map <Left> <Nop>
" map <Right> <Nop>
" map <Up> <Nop>
" map <Down> <Nop>
map Q <Nop>
map K <Nop>
cnoremap %% <C-R>=expand('%:h').'/'<cr>
nnoremap Y y$
xnoremap p pgvy

" rails
map <Leader>sc :sp db/schema.rb<cr>

" vimux
map <Leader>vc :VimuxPromptCommand<CR>
map <Leader>vl :VimuxRunLastCommand<CR>
map <Leader>vt :VimuxZoomRunner<CR>
map <Leader>vx :VimuxCloseRunner<CR>
map <Leader>vi :VumixInspectRunner<CR>
map <Leader>vu :VimuxScrollUpInspect<CR>
map <Leader>vd :VimuxScrollDownInspect<CR>
map <Leader>vp :VimuxTogglePane<CR>
map <Leader>vk :VimuxInterruptRunner<CR>

" tests
nmap <silent> <leader>trf :TestNearest<CR>
nmap <silent> <leader>tra :TestFile<CR>
nmap <silent> <leader>trs :TestSuite<CR>
nmap <silent> <leader>trl :TestLast<CR>
nmap <silent> <leader>trv :TestVisit<CR>

" Run Rubocop
nmap <Leader>rc :!rubocop %<CR>

" make test commands execute using vimux
let test#strategy = "vimux"

" Commenting blocks of code.
autocmd FileType c,cpp,java,scala let b:comment_leader = '// '
autocmd FileType sh,ruby,python   let b:comment_leader = '# '
autocmd FileType conf,fstab       let b:comment_leader = '# '
autocmd FileType tex              let b:comment_leader = '% '
autocmd FileType mail             let b:comment_leader = '> '
autocmd FileType vim              let b:comment_leader = '" '
noremap <silent> ,cc :<C-B>silent <C-E>s/^/<C-R>=escape(b:comment_leader,'\/')<CR>/<CR>:nohlsearch<CR>
noremap <silent> ,cu :<C-B>silent <C-E>s/^\V<C-R>=escape(b:comment_leader,'\/')<CR>//e<CR>:nohlsearch<CR>

" Syntastic config
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*
let g:syntastic_mode_map = { 'mode': 'active', 'active_filetypes': [],'passive_filetypes': [] }
nnoremap <C-w>E :SyntasticCheck<CR> :SyntasticToggleMode<CR>

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 0
let g:syntastic_check_on_wq = 0
let g:syntastic_html_tidy_ignore_errors=[" proprietary attribute \"ng-"]
let g:syntastic_javascript_checkers=['standard', 'jslint']
let g:syntastic_javascript_standard_exec = 'semistandard'

" set the cursor to a vertical line in insert mode and a solid block
" in command mode
if exists('$TMUX')
  let &t_SI = "\<Esc>Ptmux;\<Esc>\<Esc>]50;CursorShape=1\x7\<Esc>\\"
  let &t_EI = "\<Esc>Ptmux;\<Esc>\<Esc>]50;CursorShape=0\x7\<Esc>\\"
else
  let &t_SI = "\<Esc>]50;CursorShape=1\x7"
  let &t_EI = "\<Esc>]50;CursorShape=0\x7"
endif

" don't blink the cursor
set guicursor+=i:blinkwait0

" upon hitting escape to change modes,
" send successive move-left and move-right
" commands to immediately redraw the cursor
inoremap <special> <Esc> <Esc>hl

" Map Dash
:nmap <silent> <leader>d <Plug>DashSearch

" Map paratrooper deploying
nnoremap <leader>ds :!rake deploy:staging<cr>
nnoremap <leader>dp :!rake deploy:production<cr>

" Map copying and pasting
nmap <F2> :let currentcol=virtcol('.')<CR>^vg_"+y:cal cursor(line('.'), currentcol)<CR>
vmap <F2> "+y
nmap <F3> :set paste<CR>:r !pbpaste<CR>:set nopaste<CR>
imap <F3> <Esc>:set paste<CR>:r !pbpaste<CR>:set nopaste<CR>

let g:airline_powerline_fonts = 1

" Restore cursor position
autocmd BufReadPost *
  \ if line("'\"") > 1 && line("'\"") <= line("$") |
  \   exe "normal! g`\"" |
  \ endif

" List all functions in the current file with Tagbar
nmap <F8> :TagbarToggle<CR>

" Seeing is believing mappings
augroup seeingIsBelievingSettings
  autocmd!

  autocmd FileType ruby nmap <F5> <Plug>(seeing-is-believing-mark)
  autocmd FileType ruby xmap <F5> <Plug>(seeing-is-believing-mark)
  autocmd FileType ruby imap <F5> <Plug>(seeing-is-believing-mark)

  autocmd FileType ruby nmap <F6> <Plug>(seeing-is-believing-run)
  autocmd FileType ruby xmap <F6> <Plug>(seeing-is-believing-run)
  autocmd FileType ruby imap <F6> <Plug>(seeing-is-believing-run)

  autocmd FileType ruby nmap <F7> :%s/\s\+# =>.*$\\|^#\s[>!\~]>\(\s.*\)\?$//g \| :%s#\($\n\s*\)\+\%$##<CR>
  autocmd FileType ruby xmap <F7> :s/\s\+# =>.*$\\|^#\s[>!\~]>\(\s.*\)\?$//g \| :'<,'>s#\($\n\s*\)\+\%$##<CR>
augroup END

augroup runCodeMappings
  " Run Ruby
  autocmd FileType ruby map <Leader>rr :!ruby "%"<CR>
  autocmd FileType javascript map <Leader>rb :w !ruby -e<CR>

  " Run Javascript
  autocmd FileType javascript map <Leader>rj :!node "%"<CR>
  autocmd FileType javascript map <Leader>rb :w !node<CR>
augroup END

" Start interactive EasyAlign in visual mode (e.g. vip<Enter>)
vmap <Enter> <Plug>(EasyAlign)

" Start interactive EasyAlign for a motion/text object (e.g. gaip)
nmap ga <Plug>(EasyAlign)

let g:syntastic_javascript_checkers=['jscs', 'jshint']

" vim-easymotion config
"
" Override default find
map  / <Plug>(easymotion-sn)
omap / <Plug>(easymotion-tn)

" These `n` & `N` mappings are options. You do not have to map `n` & `N` to EasyMotion.
" Without these mappings, `n` & `N` works fine. (These mappings just provide
" different highlight method and have some other features )
map  n <Plug>(easymotion-next)
map  N <Plug>(easymotion-prev)

" easymotion motion keys
map <Leader>l <Plug>(easymotion-lineforward)
map <Leader>j <Plug>(easymotion-j)
map <Leader>k <Plug>(easymotion-k)
map <Leader>h <Plug>(easymotion-linebackward)

let g:EasyMotion_startofline = 0 " keep cursor column when JK motion

" use two character search in easymotion
nmap s <Plug>(easymotion-s2)
nmap t <Plug>(easymotion-t2)

" lorem ipsum insert
nnoremap <leader>l :Loremipsum
nnoremap <leader>li :Loremipsum! 1<CR>

" Trigger configuration. Do not use <tab> if you use https://github.com/Valloric/YouCompleteMe.
let g:UltiSnipsExpandTrigger="<C-s>"
let g:UltiSnipsJumpForwardTrigger="<c-b>"
let g:UltiSnipsJumpBackwardTrigger="<c-z>"

" If you want :UltiSnipsEdit to split your window.
let g:UltiSnipsEditSplit="vertical"

" YouCompleteMe config
let g:ycm_key_list_select_completion=[]
let g:ycm_key_list_previous_completion=[]
let g:ycm_key_list_select_completion = ['<TAB>', '<Down>']

" UltiSnips fix
set shortmess+=c

" Make sure matchit.vim is enabled
runtime macros/matchit.vim

" Gundo config
nnoremap <S-u> :GundoToggle<CR>
let g:gundo_close_on_revert=1

" NERDTree config
nnoremap <C-f> :NERDTreeToggle<cr>

let NERDTreeIgnore=[ '\.pyc$', '\.pyo$', '\.py\$class$', '\.obj$', '\.o$',
                   \ '\.so$', '\.egg$', '^\.git$', '\.cmi', '\.cmo' ]
let NERDTreeHighlightCursorline=1
let NERDTreeShowBookmarks=1
let NERDTreeShowFiles=1

" Convenience mappings
nnoremap <silent> <Leader>gd :Gdiff<CR>
nnoremap <silent> <Leader>gb :Gblame<CR>

nnoremap <Leader>a :Ag 

nmap <Leader>tf :CommandT<CR>
nmap <Leader>tj :CommandTJump<CR>
nmap <Leader>tb :CommandTBuffer<CR>
nmap <Leader>tm :CommandTMRU<CR>
nmap <Leader>tt :CommandTTag<CR>
nmap <Leader>tF :CommandTFlush<CR>
nmap <Leader>tl :CommandTLoad<CR>

" Airline setup
" Enable the list of buffers
let g:airline#extensions#tabline#enabled=1

" Show just the filename
let g:airline#extensions#tabline#fnamemod=':t'

" enable/disable displaying buffers with a single tab
let g:airline#extensions#tabline#show_buffers = 1
let g:airline#extensions#tabline#buffer_nr_show = 1

" let g:airline#extensions#tabline#left_sep = ' '
" let g:airline#extensions#tabline#left_alt_sep = '|hl'"

" Show absolute file path
nmap <C-g> :echo expand('%:p')<CR>

" configure vim-markdown
let g:vim_markdown_frontmatter = 1

" navigate every line, don't skip wrapped lin sections
nnoremap k gk
nnoremap j gj

" Configure Ack.vim
if executable('ag')
  let g:ackprg = 'ag --vimgrep'
  " other option which doesn't report each on each line
  " let g:ackprg = 'ag --nogroup --nocolor --column'
endif

" Mapping selecting mappings
nmap <leader><tab> <plug>(fzf-maps-n)
xmap <leader><tab> <plug>(fzf-maps-x)
omap <leader><tab> <plug>(fzf-maps-o)

" Insert mode completion
imap <c-x><c-k> <plug>(fzf-complete-word)
imap <c-x><c-f> <plug>(fzf-complete-path)
imap <c-x><c-j> <plug>(fzf-complete-file-ag)
imap <c-x><c-l> <plug>(fzf-complete-line)

" Advanced customization using autoload functions
inoremap <expr> <c-x><c-k> fzf#vim#complete#word({'left': '15%'})
" [Buffers] Jump to the existing window if possible
let g:fzf_buffers_jump = 1

" [[B]Commits] Customize the options used by 'git log':
let g:fzf_commits_log_options = '--graph --color=always --format="%C(auto)%h%d %s %C(black)%C(bold)%cr"'

" [Tags] Command to generate tags file
let g:fzf_tags_command = 'ctags -R'

" [Commands] --expect expression for directly executing the command
let g:fzf_commands_expect = 'alt-enter,ctrl-x'

map <Leader>. :Files<CR>
map <Leader>/ :Buffers<CR>

" Connect option-delete to <C-w>
:imap <Esc><BS> <C-w>

" Open current file in browser
nnoremap <F12>f :!open -a /Applications/Firefox.app %<CR>
nnoremap <F12>c :!open -a /Applications/Google\ Chrome.app %<CR>
nnoremap <F12>g :!open -a /Applications/Google\ Chrome.app %<CR>
nnoremap <F12>s :!open /Applications/Safari.app %'<CR>
nnoremap <F12><F12> :!open %<CR>

" let g:rooter_change_directory_for_non_project_files = 'home'
" " directories and all files (default)
" let g:rooter_targets = '/,*'
" function! s:find_git_root()
"   return system('git rev-parse --show-toplevel 2> /dev/null')[:-2]
" endfunction

" command! ProjectFiles execute 'Files' s:find_git_root()
"
" Make sure vue files are syntax highlighted correctly
autocmd FileType vue syntax sync fromstart
