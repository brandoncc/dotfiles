[ -f /usr/local/share/autojump/autojump.fish ]; and source /usr/local/share/autojump/autojump.fish

set -x LDFLAGS -L/usr/local/opt/readline/lib
set -x CPPFLAGS -I/usr/local/opt/readline/include

# Configure rbenv
set -x PATH $HOME/.rbenv/bin $PATH
set -x PATH $HOME/.rbenv/shims $PATH
set -x PATH /Applications/Postgres.app/Contents/Versions/9.6/bin $PATH
set -x PATH node_modules/.bin $PATH
rbenv init - | source

# Add misc bin directory
set -x PATH $HOME/bin $PATH

set -x PATH $HOME/go/bin $PATH

set -x EDITOR "nvim"
set -x VISUAL "nvim"

# When building rubies with ruby-build, use the updated openssl
set -x RUBY_CONFIGURE_OPTS --with-openssl-dir=(brew --prefix openssl@1.1)

set -x CXX "clang++"
set -x GYPFLAGS "-Dmac_deployment_target=10.15"

abbr -a be bundle exec
abbr -a bes bundle exec spring
abbr -a bi bundle install
abbr -a g git
abbr -a gb git branch
abbr -a gc git checkout
abbr -a gco git checkout -b
abbr -a m git checkout master
abbr -a gs git status
abbr -a o open
abbr -a v nvim
abbr -a vim nvim
abbr -a fconfig vim ~/.config/fish/config.fish
abbr -a fr omf reload
abbr -a tmux env TERM=xterm-256color tmux
abbr -a tml tmux list-sessions
abbr -a tma tmux -2 attach -t
abbr -a tmk tmux kill-session -t
abbr -a tmcc tmux -CC attach
abbr -a tmn tmux new -s
abbr -a tmr tmux rename-session -t
abbr -a acronym ruby ~/dev/scratchbox/find_acronym.rb
abbr -a dbyml wget https://gist.githubusercontent.com/brandoncc/48a869862cdeed5a130b/raw/8491d5cba32f08df8563c155a7b6c42570693986/myflix_db.yml -O config/database.yml
abbr -a mux tmuxinator start
abbr -a rst "rm tmp/cache/webpacker/.last-compilation-digest; rails test:system"
abbr -a fishconfig code ~/.config/fish
abbr -a ut update_tlm
abbr -a dcr docker-compose run --rm
abbr -a dcbu docker-compose up --build
abbr -a dcu docker-compose up
abbr -a dcb docker-compose build

function ogf --no-scope-shadowing
    echo "Cloning, your editor will open when clone has completed..."
    source (env TARGET_DIRECTORY=~/students EDITOR=$EDITOR clone_git_file -ts $argv[1] | psub)
end

function wogf
    set -l EDITOR webstorm
    ogf $argv
end

function vogf
    set -l EDITOR vim
    ogf $argv
end

function chext
    if test (count $argv) -eq 2
        for f in *$argv[1]
            mv $f (string replace -r \.$argv[1]\$ .$argv[2] $f)
        end
    else
        echo "Error: use the following syntax"
        echo "chext oldext newext"
    end
end

function killport
    if test (count $argv) -eq 1
        if test (lsof -i :$argv[1] | awk '{print $2}' | tail -1 | wc -l) -eq 1
            set -l COMMAND (lsof -i :$argv[1] | awk '{print $1}' | tail -1)
            kill -9 (lsof -i :$argv[1] | awk '{print $2}' | tail -1)
            echo "Command '$COMMAND' killed"
        else
            echo "There are no applications using port $argv[1]"
        end
    else
        echo "Error: You must provide a port number"
    end
end

function update_tlm
    set repo $argv[1]

    if not test (count $argv) -eq 1
        echo "Error: You must provide a repo name"
        return 1
    end

    if not test -d ~/dev/ls/$repo
        echo "The directory you specified ($repo) does not exist. You should clone it instead."
        return 1
    end

    cd ~/dev/ls/$repo

    echo "====> Fetching all branches from all remotes..."
    git fetch

    echo "
  
  ====> Checking out master..."
    git checkout master

    echo "
  
  ====> Pulling origin/master..."
    git pull origin master

    echo "
  
  ====> Pulling TLM subtree..."
    git subtree pull --prefix vendor/tealeaf_markup git@github.com:gotealeaf/tealeaf_markup.git master --squash

    echo "


  
  ====> Pushing master to origin..."

    if read_confirm
        git push origin master
    else
        echo "Push cancelled, but the changes are still applied locally."
    end

    cd -
end

# ref: https://stackoverflow.com/questions/16407530/how-to-get-user-confirmation-in-fish-shell
function read_confirm
    while true
        read -l -P 'Do you want to continue? [y/N] ' confirm

        switch $confirm
            case Y y
                return 0
            case '' N n
                return 1
        end
    end
end

function commit_advantage2
    pushd /Users/brandoncc/dev/keyboard-configuration/

    cp -af /Volumes/ADVANTAGE2/active/ ./advantage2/
    git add .
    git commit
    git push origin master

    popd
end

function commit_dotfiles
    pushd /Users/brandoncc/dotfiles

    git add .
    git commit
    git push origin master

    popd
end

# edit vim config
function evc
    code ~/.config/nvim/init.vim
end

function sfc
    source ~/.config/fish/config.fish
end

function efc
    code -wn ~/.config/fish/config.fish
    sfc
end
